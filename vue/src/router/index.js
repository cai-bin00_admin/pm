import Vue from 'vue'
import Router from 'vue-router'
//首页路由
import Home from '@/components/common/Home'
//统计图表
import Count from '@/components/pages/count/index'
//主页重定向路由
import Index from '@/components/common/index'
//用户管理路由
import UserList from '@/components/pages/user/UserList'
//员工管理路由
import Employee from '@/components/pages/Employee/index'
import dEmployee from '@/components/pages/Employee/depart'
import tEmployee from '@/components/pages/Employee/temporary'
import Car from '@/components/pages/Employee/car'
//登录路由
import Login from '@/components/common/Login'
//系统管理路由
import SysMenu from '@/components/pages/sys/SysMenu'
import SysLogin from '@/components/pages/sys/SysLogin'
import SysLog from '@/components/pages/sys/SysLog'
//注册路由
import register from '@/components/common/register'

Vue.use(Router)

export default new Router({
  routes: [
    {//主页
      path: '/Home',
      component: Home,
      redirect: '/',
      children: [
        {//主页重定向
          path: '/Home',
          name: 'index',
          component: Index
        },
        {//统计图表
          path: '/Count',
          name: 'count',
          component: Count
        },
        {//菜单管理
          path: '/SysMenu',
          name: 'SysMenu',
          component: SysMenu
        },
        {//登录记录管理
          path: '/SysLogin',
          name: 'SysLogin',
          component: SysLogin
        },
        {//操作记录管理
          path: '/SysLog',
          name: 'SysLog',
          component: SysLog
        },
        {//用户管理
          path: '/UserList',
          name: 'UserList',
          component: UserList
        },
        {//员工管理
          path: '/Employee',
          name: 'Employee',
          component: Employee
        },
        {//离职员工管理
          path: '/dEmployee',
          name: 'dEmployee',
          component: dEmployee
        },
        {//临时员工管理
          path: '/tEmployee',
          name: 'tEmployee',
          component: tEmployee
        },
        {//车辆管理
          path: '/EmployeeCar',
          name: 'Car',
          component: Car
        },
      ]
    },
    {//登录
      path: '/',
      name: 'login',
      component: Login,
    },
    {//注册
      path: '/register',
      name: 'register',
      component: register,
    },
  ]
})
