import Vue from 'vue'
import App from './App'
import ElementUI, {Message, Table, TableColumn} from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VForm from 'vform-builds' //引入VForm库
import router from './router'
import store from './store'
import method from './method'
import Axios from 'axios'
import * as echarts from 'echarts'
import 'vform-builds/dist/VFormDesigner.css' //引入VForm样式
import api from "./api";
import {export_json_to_excel} from './excel/Export2Excel'
//注册组件
Vue.prototype.export_json_to_excel = export_json_to_excel
Vue.prototype.$echarts = echarts
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(VForm)  //全局注册VForm(同时注册了v-form-designer和v-form-render组件)
Vue.use(method)
//配置axios
Vue.prototype.$http = Axios
//默认路径
Axios.defaults.baseURL = 'http://localhost:8889/'
// Axios.defaults.baseURL = 'http://43.138.23.222:8889/'

// 获取组件的props
const TableProps = Table.props
const TableColumnProps = TableColumn.props

// 修改默认props
// 全局el-table设置
TableProps.border.default = true // 边框
// 全局el-table-column设置
TableColumnProps.align.default = 'center' // 居中
TableColumnProps.showOverflowTooltip.default = true // 文本溢出

TableProps.border = {type: Boolean, default: true} // 边框

Axios.interceptors.request.use(config => {
  const token = sessionStorage.getItem('token')
  const tokenName = sessionStorage.getItem('tokenName')
  config.headers[tokenName] = token // 请求头带上Token
  config.headers["Browser"] = api.getBrowser()
  // config.headers["Resolution"]=api.getResolution()
  // config.headers["colorDepth"]=api.getColorDepth()
  config.headers["OS"] = api.getOS()
  return config;
}, error => {
  return Promise.reject(error);
})

/** 响应拦截器 */
Axios.interceptors.response.use((success) => {
  const response = success.data
  if (success.status && success.status === 200) {
    if (response.message) {
      Message({type: response.type, message: response.message});
    }
    if (response.code === 11012) {   //token无效
      router.push("/").then(null)
      return;
    }
    // if (response.code === 500 || response.code === 401 || response.code === 403) {   //接口请求成功，业务逻辑错误
    //   Message.error({message: response.message});
    //   return;
    // }
  }
  return success.data.data;
}, (error) => {
  const response = error.response
  if (response.status === 504 || response.status === 404) {
    Message.error({message: '服务器被吃了( ╯□╰ )'});
  } else if (response.status === 403) {
    Message.error({message: '权限不足，请联系管理员'});
  } else if (response.status === 40 || response.status === 500) {
    Message.error({message: '请登录账号'});
    router.push('/').then(null);
  } else {
    if (response.message) {
      Message({type: response.type, message: response.message});
    } else {
      Message({message: '该错误触及盲区(●ˇ∀ˇ●)'});
    }
  }
});
new Vue({
  el: '#app',
  router,//挂载router
  store,//挂载store
  components: {App},
  template: '<App/>'
})
