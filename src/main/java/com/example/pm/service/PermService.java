package com.example.pm.service;


import com.example.pm.common.result.R;
import com.example.pm.domain.Perm;

import java.util.List;

public interface PermService {
    void createPermRedis();

    List<Perm> getList();

    List<String> getListByZh(String zh);

    R<String> changePermForEmployee(String zh, List<String> perm);
}
