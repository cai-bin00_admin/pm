package com.example.pm.service;

import java.util.Map;

public interface CountService {
    Map<String, Object> NewEmployeesEveryMonthThisYear();

    Map<String, Object> TextTable();

    Map<String, Object> NewEmployeesEveryYear();
}
