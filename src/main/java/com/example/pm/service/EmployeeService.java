package com.example.pm.service;

import com.example.pm.common.result.R;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Employee;
import com.example.pm.domain.Image;
import com.example.pm.domain.Vo.PageVo;

public interface EmployeeService {
    R<PageVo<Object>> getEmployees(PageBo pageBo, String jzzt);

    R<PageVo<Object>> getEmployeeBySearch(Employee employee, PageBo pageBo, String jzzt);

    R<Object> EmployeeSearchOption();

    R<Object> insertEmployee(Employee employee, PageBo pageBo);

    R<Object> deleteEmployee(String id, PageBo pageBo);

    R<PageVo<Object>> updateEmployee(Employee employee, PageBo pageBo);

    R<Object> getXxdjb(Integer id);

    R<Object> insertXxdjb(Image image);

    R<Object> getXsxwzs(Integer id);

    R<Object> insertXsxwzs(Image image);

    R<Object> getByzs(Integer id);

    R<Object> insertByzs(Image image);

    R<Object> getZczs(Integer id);

    R<Object> insertZczs(Image image);

    R<Object> getSfz(Integer id);

    R<Object> insertSfz(Image image);

    R<Object> getJsb(Integer id);

    R<Object> insertJsb(Image image);
}
