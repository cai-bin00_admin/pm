package com.example.pm.service;


import com.example.pm.common.result.R;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Menu;
import com.example.pm.domain.Vo.PageVo;

import java.util.List;

public interface MenuService {

    List<Menu> getAsideMenu();

    PageVo<Menu> getPage(PageBo pageBo);

    List<Menu> listParent();

    R<PageVo<Menu>> insert(Menu menu, PageBo pageBo);

    R<PageVo<Menu>> update(Menu menu, PageBo pageBo);

    R<PageVo<Menu>> delete(Menu menu, PageBo pageBo);

    PageVo<Menu> getPageBySearch(Menu menu, PageBo pageBo);

    R<List<Integer>> getListByZh(String zh);

    R<List<Menu>> getList();

    R<String> changeMenuForEmployee(String zh, List<Integer> newMenu, List<Integer> oldMenu);
}
