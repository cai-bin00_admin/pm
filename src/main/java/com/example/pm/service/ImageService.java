package com.example.pm.service;


public interface ImageService {
    String getImageById(int id);
}
