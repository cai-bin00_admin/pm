package com.example.pm.service;


import com.example.pm.common.result.R;
import com.example.pm.domain.Notice;

import java.util.List;

public interface NoticeService {
    R<List<Notice>> getNotice();
}
