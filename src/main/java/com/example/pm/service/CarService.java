package com.example.pm.service;


import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Car;
import com.example.pm.domain.Vo.PageVo;

import java.util.List;

public interface CarService {
    PageVo<Car> getCars(PageBo pageBo);

    PageVo<Car> getCarBySearch(Car car, PageBo pageBo);

    void createCarRedis();

    void insertList(List<Car> lists);
}
