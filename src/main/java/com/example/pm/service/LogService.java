package com.example.pm.service;

import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Log;
import com.example.pm.domain.Vo.PageVo;

public interface LogService {
    void insertLog(Log log);

    PageVo<Log> getList(PageBo pageBo);
}
