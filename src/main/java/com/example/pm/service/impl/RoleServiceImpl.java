package com.example.pm.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.pm.common.result.R;
import com.example.pm.domain.Role;
import com.example.pm.mapper.RoleMapper;
import com.example.pm.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<Map<String, Object>> getRoleMap() {
        List<Role> roles = roleMapper.selectList(new LambdaQueryWrapper<>());
        List<Map<String, Object>> list = new ArrayList<>();
        roles.stream().peek(o -> {
            Map<String, Object> map = new HashMap<>();
            map.put("id", o.getId());
            map.put("name", o.getName());
            list.add(map);
        }).collect(Collectors.toList());
        return list;
    }

    @Override
    public R<List<Role>> getRoleList() {
        List<Role> roleList = roleMapper.selectList(new LambdaQueryWrapper<>());
        roleList = roleList.stream().map(o -> {
            Role role = new Role();
            role.setId(o.getId());
            role.setName(o.getName());
            return role;
        }).collect(Collectors.toList());
        return R.ok(roleList);
    }

    @Override
    public R<Integer> getRoleIdByZh(String zh) {
        Role role = roleMapper.getByZh(zh);
        return R.ok(role.getId());
    }
}
