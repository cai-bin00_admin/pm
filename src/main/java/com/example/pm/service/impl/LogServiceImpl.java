package com.example.pm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Log;
import com.example.pm.domain.Vo.PageVo;
import com.example.pm.mapper.EmployeeMapper;
import com.example.pm.mapper.LogMapper;
import com.example.pm.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogMapper logMapper;
    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public void insertLog(Log log) {
        logMapper.insertLog(log);
    }

    @Override
    public PageVo<Log> getList(PageBo pageBo) {
        LambdaQueryWrapper<Log> lqw = new LambdaQueryWrapper<>();
        lqw.orderByDesc(Log::getId);
        List<Log> logs = logMapper.selectList(lqw);
        return new PageVo<>(pageBo, logs);
    }
}
