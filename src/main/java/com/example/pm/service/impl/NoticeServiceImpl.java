package com.example.pm.service.impl;


import com.example.pm.common.result.R;
import com.example.pm.domain.Notice;
import com.example.pm.mapper.NoticeMapper;
import com.example.pm.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class NoticeServiceImpl implements NoticeService {
    @Autowired
    private NoticeMapper noticeMapper;

    @Override
    public R<List<Notice>> getNotice() {
        List<Notice> list = noticeMapper.getNotice(LocalDateTime.now());
        return R.ok(list);
    }
}
