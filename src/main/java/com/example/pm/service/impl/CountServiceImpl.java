package com.example.pm.service.impl;

import com.example.pm.domain.Employee;
import com.example.pm.mapper.EmployeeMapper;
import com.example.pm.service.CountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CountServiceImpl implements CountService {
    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 获取当前年份
     */
    public static int getThisYear() {
        Calendar date = Calendar.getInstance();
        return date.get(Calendar.YEAR);
    }

    /**
     * 获取当前月份
     */
    public static int getThisMonth() {
        Calendar date = Calendar.getInstance();
        return date.get(Calendar.MONTH) + 1;
    }

    @Override
    public Map<String, Object> NewEmployeesEveryMonthThisYear() {
        int year = getThisYear();
        int month = getThisMonth();
        List<String> months = new ArrayList<>();
        List<Integer> counts = new ArrayList<>();

        for (int i = 1; i <= month; i++) {
//            String YY = year + "-";
//            if (i < 10) YY += "0";
//            YY += i;
//            LambdaQueryWrapper<Employee> lqw = new LambdaQueryWrapper<>();
//            lqw.like(Employee::getRzrq, YY);
//            int count = employeeMapper.selectList(lqw).size();
//            months.add(YY);

            String rzrq = year + "-";
            rzrq += i;
            int count = employeeMapper.countByRzrq(rzrq);
            months.add(rzrq);
            counts.add(count);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("month", months);
        map.put("count", counts);
        return map;
    }

    @Override
    public Map<String, Object> TextTable() {
        String lastYear = (getThisYear() - 1) + "";
        int lastYearNumber = employeeMapper.countByRzrq(lastYear);
        String thisYear = getThisYear() + "";
        int thisYearNumber = employeeMapper.countByRzrq(thisYear);
        String thisMonth = thisYear + "-" + getThisMonth();
        int thisMonthNumber = employeeMapper.countByRzrq(thisMonth);
        int thisMonthLiZhiNumber = employeeMapper.countByLzrq(thisMonth);
        Map<String, Object> map = new HashMap<>();
        map.put("lastYear", lastYearNumber);
        map.put("thisYear", thisYearNumber);
        map.put("thisMonth", thisMonthNumber);
        map.put("thisMonthLiZhi", thisMonthLiZhiNumber);
        return map;
    }

    @Override
    public Map<String, Object> NewEmployeesEveryYear() {
        List<Employee> employeeList = employeeMapper.getEmployees();
        List<Integer> years = employeeList.stream().filter(o -> o.getRzrq() != null)
                .map(m -> Integer.valueOf(m.getRzrq().toString().split("-")[0])).distinct().sorted().collect(Collectors.toList());
        Integer firstYear = years.get(0);
        Integer lastYear = years.get(years.size() - 1);

        years = new ArrayList<>();
        List<Long> counts = new ArrayList<>();

        for (int i = firstYear; i <= lastYear; i++) {
            Integer year = i;
            long count = employeeList.stream().filter(o -> o.getRzrq() != null)
                    .filter(o -> year.equals(Integer.valueOf(o.getRzrq().toString().split("-")[0]))).distinct().count();
            years.add(year);
            counts.add(count);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("year", years);
        map.put("count", counts);
        return map;
    }
}
