package com.example.pm.service.impl;


import com.example.pm.common.constant.RedisConstant;
import com.example.pm.common.redis.RedisUtils;
import com.example.pm.common.utils.GzipUtils;
import com.example.pm.common.utils.StringUtils;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Car;
import com.example.pm.domain.Vo.PageVo;
import com.example.pm.mapper.CarMapper;
import com.example.pm.mapper.EmployeeMapper;
import com.example.pm.mapper.ImageMapper;
import com.example.pm.mapper.UserMapper;
import com.example.pm.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {
    @Autowired
    private CarMapper carMapper;
    @Autowired
    private GzipUtils gzipUtils;
    String key = RedisConstant.getList_Car;
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ImageMapper imageMapper;
    @Autowired
    private RedisUtils redisUtils;

    @Override
    public PageVo<Car> getCars(PageBo pageBo) {
        List<Car> list = (List<Car>) redisUtils.get(key);
        return new PageVo<>(pageBo, list);
    }

    @Override
    public PageVo<Car> getCarBySearch(Car car, PageBo pageBo) {
        List<Car> list = (List<Car>) redisUtils.get(key);
        list = list.stream().filter(o -> (StringUtils.isNotEmpty(o.getXm()) && o.getXm().contains(car.getXm()))).collect(Collectors.toList());
        list = list.stream().filter(o -> (StringUtils.isNotEmpty(o.getCph()) && o.getCph().contains(car.getCph()))).collect(Collectors.toList());
        list = list.stream().filter(o -> (StringUtils.isNotEmpty(o.getSjh()) && o.getSjh().contains(car.getSjh()))).collect(Collectors.toList());
        return new PageVo<>(pageBo, list);
    }

    @Async
    @Override
    public void createCarRedis() {
        List<Car> list = carMapper.getAllCars();
        list = list.stream().peek(o -> {
            o.setImage(gzipUtils.compress(o.getImagestr()));
            o.setImagestr("");
        }).collect(Collectors.toList());
        long time = RedisConstant.getList_Car_Time;
        redisUtils.set(key, list, time);
    }

    @Async
    @Override
    public void insertList(List<Car> lists) {
        carMapper.insertList(lists);
    }

}
