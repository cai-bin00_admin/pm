package com.example.pm.service;


import com.example.pm.common.result.R;
import org.springframework.web.multipart.MultipartFile;

public interface ImportService {
    R<String> importEmployees(MultipartFile[] files);

    R<String> importCars(MultipartFile[] files);
}
