package com.example.pm.service;

import com.example.pm.common.result.R;
import lombok.SneakyThrows;

import java.util.Map;

public interface CodeService {
    @SneakyThrows
    R<Map<String, Object>> getCode();
}
