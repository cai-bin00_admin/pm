package com.example.pm.service;


import com.example.pm.domain.Dict;

import java.util.List;

public interface DictService {

    List<Dict> getDict(String dict);

}
