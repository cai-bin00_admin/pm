package com.example.pm.service;


import com.example.pm.common.result.R;
import com.example.pm.domain.Role;

import java.util.List;
import java.util.Map;

public interface RoleService {
    List<Map<String, Object>> getRoleMap();

    R<List<Role>> getRoleList();

    R<Integer> getRoleIdByZh(String zh);
}
