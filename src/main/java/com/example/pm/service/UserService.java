package com.example.pm.service;


import com.example.pm.common.result.R;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.User;
import com.example.pm.domain.Vo.PageVo;

import java.util.Map;

public interface UserService {
    PageVo<User> getUserPage(PageBo pageBo);

    byte[] getUserImage();

    Map<String, Object> getInfo();

    R<Object> getUserBySearch(User user, PageBo pageBo);

    R<Object> changeZt(String zh, PageBo pageBo);

    void createUserRedis();

    R<String> changeRole(User user);
}
