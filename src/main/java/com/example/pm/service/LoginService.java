package com.example.pm.service;


import com.example.pm.common.result.R;
import com.example.pm.domain.Bo.LoginBo;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Login;
import com.example.pm.domain.Vo.PageVo;

import java.util.List;

public interface LoginService {

    R<Object> doLogin(LoginBo loginBo);

    PageVo<Login> getLoginList(PageBo pageBo);

    R<PageVo<Login>> getLoginListBySearch(Login login, PageBo pageBo);

    List<Login> getListIndex();
}
