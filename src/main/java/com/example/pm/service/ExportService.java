package com.example.pm.service;


import com.example.pm.common.result.R;

import java.util.List;
import java.util.Map;

public interface ExportService {
    R<Map<String, Object>> exportEmployees(List<Integer> ids);

    R<Map<String, Object>> exportCars();
}
