package com.example.pm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.pm.domain.Role;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RoleMapper extends BaseMapper<Role> {
    Role getByZh(String zh);
}
