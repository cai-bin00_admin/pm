package com.example.pm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.pm.domain.Model.UserPerm;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface UserPermMapper extends BaseMapper<UserPerm> {
    void insertList(List<UserPerm> list);
}
