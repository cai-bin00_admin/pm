package com.example.pm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.pm.domain.Car;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CarMapper extends BaseMapper<Car> {
    void insertList(@Param("list") List<Car> list);

    List<Car> getAllCars();

}
