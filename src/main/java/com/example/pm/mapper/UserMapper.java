package com.example.pm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.pm.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    void insertList(@Param("list") List<User> userList);

    void insertUser(User user);

}
