package com.example.pm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.pm.domain.Login;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface LoginMapper extends BaseMapper<Login> {
    void insertLogin(@Param("login") Login login);
}
