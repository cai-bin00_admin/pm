package com.example.pm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.pm.domain.Notice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {
    void deleteByTime(LocalDateTime now);

    void insertList(@Param("list") List<Notice> list);

    List<Notice> getNotice(LocalDateTime now);
}
