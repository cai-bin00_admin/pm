package com.example.pm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.pm.domain.Image;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ImageMapper extends BaseMapper<Image> {
    String getImageByZh(String zh);

    String getXxdjb(Integer id);

    void insertXxdjb(@Param("image") Image image);

    String getXsxwzs(Integer id);

    void insertXsxwzs(@Param("image") Image image);

    String getByzs(Integer id);

    void insertByzs(@Param("image") Image image);

    String getZczs(Integer id);

    void insertZczs(@Param("image") Image image);

    String getSfz(Integer id);

    void insertSfz(@Param("image") Image image);

    String getJsb(Integer id);

    void insertJsb(@Param("image") Image image);
}
