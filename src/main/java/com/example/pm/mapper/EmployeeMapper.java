package com.example.pm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.pm.domain.Employee;
import com.example.pm.domain.Notice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
    List<Employee> getEmployees();

    void updateList(@Param("list") List<Employee> list);

    void insertList(List<Employee> lists);

    int countByRzrq(String date);

    int countByLzrq(String date);

    Employee getByZh(String zh);

    void insertEmployee(Employee employee);

    Integer getLastDah();

    //查询没有档案信息的员工
    List<Notice> selectListOfNotInfo();
}
