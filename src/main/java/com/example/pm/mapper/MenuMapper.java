package com.example.pm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.pm.domain.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MenuMapper extends BaseMapper<Menu> {
    void updateMenuList(@Param("list") List<Menu> menus);

    List<Menu> selectMenuList();
}
