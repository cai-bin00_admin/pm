package com.example.pm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.pm.domain.Model.EmployeeMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface EmployeeMenuMapper extends BaseMapper<EmployeeMenu> {
    void insertList(@Param("list") List<EmployeeMenu> list);

    int deleteList(@Param("list") List<EmployeeMenu> list);
}
