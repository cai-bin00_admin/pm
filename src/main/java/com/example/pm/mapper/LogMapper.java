package com.example.pm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.pm.domain.Log;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface LogMapper extends BaseMapper<Log> {
    void insertLog(@Param("log") Log log);
}
