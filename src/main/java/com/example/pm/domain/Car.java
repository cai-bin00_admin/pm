package com.example.pm.domain;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.pm.common.basic.expect.expectToExcel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 车辆表
 */
@Data
@TableName("sys_car")
@Api(value = "车辆实体", tags = {"车辆实体"})
public class Car {
    @ApiModelProperty("编号")
    @NotNull(message = "[编号]不能为空")
    @expectToExcel(header = "编号")
    private Integer id;

    @ApiModelProperty("姓名")
    @expectToExcel(header = "姓名")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String xm;

    @ApiModelProperty("车牌号")
    @expectToExcel(header = "车牌号")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String cph;

    @ApiModelProperty("手机号")
    @expectToExcel(header = "手机号")
    private String sjh;

    @ApiModelProperty("部门")
    @expectToExcel(header = "部门")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String bm;

    @TableField(exist = false)
    @ApiModelProperty("员工头像Str")
    @expectToExcel(expect = false)
    private String imagestr;

    @TableField(exist = false)
    @ApiModelProperty("员工头像")
    @expectToExcel(expect = false)
    private byte[] image;

}
