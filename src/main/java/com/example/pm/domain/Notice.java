package com.example.pm.domain;


import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("sys_notice")
@Api(value = "系统通知实体", tags = {"系统通知实体"})
public class Notice {

    @ApiModelProperty("登录编号")
    private Integer id;

    @ApiModelProperty("通知类型")
    private String type;

    @ApiModelProperty("通知信息")
    private String message;

    @ApiModelProperty("时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime time;
}

