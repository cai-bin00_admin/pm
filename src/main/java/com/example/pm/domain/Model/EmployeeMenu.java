package com.example.pm.domain.Model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("sys_employee_menu")
@Api(value = "员工跟菜单对应实体", tags = {"员工跟菜单对应实体"})
public class EmployeeMenu {
    @TableId
    @ApiModelProperty("id")
    private Integer id;
    @ApiModelProperty("菜单ID")
    private Integer menuId;
    @ApiModelProperty("员工账号")
    private String zh;
    @ApiModelProperty("对应关系")
    private String relationship;
}
