package com.example.pm.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@TableName("sys_user")
@Api(value = "员工信息表", tags = {"员工信息表"})
public class User {
    @TableId
    @ApiModelProperty("主键")
    @NotNull(message = "[主键]不能为空")
    private Integer id;

    @ApiModelProperty("账号")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String zh;

    @ApiModelProperty("密码")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String mm;

    @ApiModelProperty("盐值")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String yz;

    @ApiModelProperty("账号状态（正常，禁用）")
    private String zt;

    @ApiModelProperty("对应的员工序号")
    private Integer employeeId;

    @ApiModelProperty("头像id")
    private Integer imageId;

    @TableField(exist = false)
    @ApiModelProperty("头像编码")
    private String image;

    @ApiModelProperty("角色id")
    private Integer roleId;

    @ApiModelProperty("对应的员工姓名")
    private String xm;

}
