package com.example.pm.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@TableName("sys_menu")
@Api(value = "菜单管理", tags = {"菜单管理"})
public class Menu implements Serializable {
    @TableId
    @NotNull(message = "[菜单ID]不能为空")
    @ApiModelProperty("菜单ID")
    private Integer menuId;

    @ApiModelProperty("父菜单ID，一级菜单为0")
    private Integer parentId;

    @Size(max = 50, message = "编码长度不能超过50")
    @ApiModelProperty("菜单名称")
    @Length(max = 50, message = "编码长度不能超过50")
    private String name;

    @Size(max = 200, message = "编码长度不能超过200")
    @ApiModelProperty("菜单URL")
    @Length(max = 200, message = "编码长度不能超过200")
    private String url;

    @ApiModelProperty("类型   0：目录   1：菜单   2：按钮")
    private Integer type;

    @ApiModelProperty("菜单图标")
    @Size(max = 50, message = "编码长度不能超过50")
    @Length(max = 50, message = "编码长度不能超过50")
    private String icon;

    @ApiModelProperty("排序")
    private Integer orderNum;

    @ApiModelProperty("子菜单")
    @TableField(exist = false)
    private List<Menu> children;

    @ApiModelProperty("所需角色权限")
    private Integer access;

    @ApiModelProperty("对应的角色")
    @TableField(exist = false)
    private Integer roleId;
}
