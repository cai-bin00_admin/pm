package com.example.pm.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;


@Data
@TableName("sys_log")
@Api(value = "日志实体", tags = {"日志实体"})
public class Log {
    @ApiModelProperty("主键")
    private Integer id;

    @ApiModelProperty("操作账户")
    private String account;

    @ApiModelProperty("操作者姓名")
    private String xm;

    @ApiModelProperty("请求方式")
    private String method;

    @ApiModelProperty("请求地址")
    private String url;

    @ApiModelProperty("请求接口")
    private String uri;

    @ApiModelProperty("请求参数")
    private String params;

    @ApiModelProperty("执行时长")
    private Double time;

    @ApiModelProperty("ip地址")
    private String ip;

    @ApiModelProperty("操作时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty("请求映射控制类")
    private String ControlClass;

    @ApiModelProperty("请求来源")
    private String innerIp;

    @ApiModelProperty("设备来源")
    private String mobile;

    @ApiModelProperty("返回提示类型")
    private String type;

    @ApiModelProperty("返回状态")
    private Integer code;

    @ApiModelProperty("返回提示")
    private String message;

    @ApiModelProperty("返回结果")
    private String result;

    @ApiModelProperty("操作系统")
    private String os;

    @ApiModelProperty("浏览器信息")
    private String browser;

    @ApiModelProperty("操作说明")
    private String remark;

}
