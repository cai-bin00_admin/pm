package com.example.pm.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.pm.common.basic.expect.expectToExcel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("sys_employee")
@Api(value = "员工信息表", tags = {"员工信息表"})
public class Employee implements Serializable {
    @TableId
    @ApiModelProperty("主键")
    @expectToExcel(header = "序号")
    private Integer id;

    @ApiModelProperty("姓名")
    @expectToExcel(header = "姓名")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String xm;

    @ApiModelProperty("档案号")
    @expectToExcel(header = "档案号")
    private Integer dah;

    @ApiModelProperty("部门")
    @expectToExcel(header = "部门")
    @Size(max = 10, message = "编码长度不能超过10")
    @Length(max = 10, message = "编码长度不能超过10")
    private String bm;

    @ApiModelProperty("性别")
    @expectToExcel(header = "性别")
    @Size(max = 10, message = "编码长度不能超过10")
    @Length(max = 10, message = "编码长度不能超过10")
    private String xb;

    @ApiModelProperty("职务")
    @expectToExcel(header = "职务")
    @Size(max = 10, message = "编码长度不能超过10")
    @Length(max = 10, message = "编码长度不能超过10")
    private String zw;

    @ApiModelProperty("入职日期")
    @expectToExcel(header = "入职日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime rzrq;

    @ApiModelProperty("离职日期")
    @expectToExcel(header = "离职日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime lzrq;

    @ApiModelProperty("就职状态")
    @expectToExcel(header = "就职状态")
    @Size(max = 10, message = "编码长度不能超过10")
    @Length(max = 10, message = "编码长度不能超过10")
    private String jzzt;

    @ApiModelProperty("年龄")
    @expectToExcel(header = "年龄")
    private Integer nl;

    @ApiModelProperty("毕业学校")
    @expectToExcel(header = "毕业学校")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String byxx;

    @ApiModelProperty("学历")

    @expectToExcel(header = "学历")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String xl;

    @ApiModelProperty("专业")
    @expectToExcel(header = "专业")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String zy;

    @ApiModelProperty("职称及证件")
    @expectToExcel(header = "职称及证件")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String zcjzj;

    @ApiModelProperty("籍贯")
    @expectToExcel(header = "籍贯")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String jg;

    @ApiModelProperty("民族")
    @expectToExcel(header = "民族")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String mz;

    @ApiModelProperty("身份证")
    @expectToExcel(header = "身份证")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String sfz;

    @ApiModelProperty("出生日期")
    @expectToExcel(header = "出生日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime csrq;

    @ApiModelProperty("户口所在地")
    @expectToExcel(header = "户口所在地")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String hkszd;

    @ApiModelProperty("现住址")
    @expectToExcel(header = "现住址")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String xzz;

    @ApiModelProperty("合同")
    @expectToExcel(header = "合同")
    @Size(max = 255, message = "编码长度不能超过255")
    @Length(max = 255, message = "编码长度不能超过255")
    private String ht;

    @ApiModelProperty("合同开始")
    @expectToExcel(header = "合同开始")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime htks;

    @ApiModelProperty("合同结束")
    @expectToExcel(header = "合同结束")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime htjs;

    @ApiModelProperty("手机号")
    @expectToExcel(header = "手机号")
    private String sjh;

    @ApiModelProperty("银行卡号")
    @expectToExcel(header = "银行卡号")
    private String yhkh;

    @ApiModelProperty("银行卡备注")
    @expectToExcel(header = "备注")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String yhkbz;

    @ApiModelProperty("招聘来源")
    @expectToExcel(header = "招聘来源")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String zply;

    @ApiModelProperty("紧急联系人")
    @expectToExcel(header = "紧急联系人")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String jjlxr;

    @ApiModelProperty("紧急联系人电话")
    @expectToExcel(header = "紧急联系人电话")
    private String jjlxrdh;

    @ApiModelProperty("入厂年限")
    @expectToExcel(header = "入厂年限")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String rcnx;

    @ApiModelProperty("保密协议")
    @expectToExcel(header = "保密协议")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String bmxy;

    @ApiModelProperty("头像id")
    @expectToExcel(expect = false)
    private Integer txid;
}
