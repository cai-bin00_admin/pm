package com.example.pm.domain;


import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@TableName("sys_login")
@Api(value = "登录记录实体", tags = {"登录记录实体"})
public class Login {

    @ApiModelProperty("登录编号")
    @NotNull(message = "[登录编号]不能为空")
    private Integer id;

    @ApiModelProperty("账号")
    @Size(max = 11, message = "编码长度不能超过11")
    @Length(max = 11, message = "编码长度不能超过11")
    private String account;

    @ApiModelProperty("姓名")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String xm;

    @ApiModelProperty("登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time;

    @ApiModelProperty("登录地址")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String ip;

    @ApiModelProperty("访问设备")
    @Size(max = 10, message = "编码长度不能超过10")
    @Length(max = 10, message = "编码长度不能超过10")
    private String mobile;

    @ApiModelProperty("访问来源")
    @Size(max = 10, message = "编码长度不能超过10")
    @Length(max = 10, message = "编码长度不能超过10")
    private String innerIp;

}

