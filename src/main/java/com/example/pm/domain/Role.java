package com.example.pm.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@TableName("sys_role")
@Api(value = "角色实体", tags = {"角色实体"})
public class Role {
    @TableId
    @NotNull(message = "[角色编号]不能为空")
    @ApiModelProperty("角色编号")
    private Integer id;

    @ApiModelProperty("角色名称")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String name;

    @ApiModelProperty("备注")
    @Size(max = 100, message = "编码长度不能超过100")
    @Length(max = 100, message = "编码长度不能超过100")
    private String remark;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty("角色权限(1最高)")
    private Integer access;
}
