package com.example.pm.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@TableName("sys_image")
@Api(value = "图片表", tags = {"图片表"})
public class Image implements Serializable {
    @TableId
    @ApiModelProperty("图片序号")
    @NotNull(message = "[图片序号]不能为空")
    private Integer id;

    @ApiModelProperty("图片编码")
    @NotBlank(message = "[图片编码]不能为空")
    private String image;

    @TableField(exist = false)
    @ApiModelProperty("对应的员工id")
    private Integer employeeId;

}
