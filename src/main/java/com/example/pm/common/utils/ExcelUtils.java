package com.example.pm.common.utils;


import cn.hutool.core.date.LocalDateTimeUtil;
import com.example.pm.common.constant.SystemConstant;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;

import static org.apache.poi.ss.usermodel.CellType.STRING;

public class ExcelUtils {
    /**
     * 导入Excel文件
     *
     * @return
     */
    public static List<String> uploadExcel(MultipartFile[] files) {
        List<String> lists = new ArrayList<>();
        for (MultipartFile file : files) {
            String[] dayTime = LocalDateTimeUtil.formatNormal(LocalDateTime.now()).replace(":", "-").split(" ");
            String day = dayTime[0];
            String time = dayTime[1];
            String name = file.getOriginalFilename();//获取文件名
            String path = SystemConstant.FILE_Upload + day + "\\" + time + "\\";
            // 检测是否存在该目录
            if (!new File(path).exists()) {
                new File(path).mkdirs();
            }
            path = path + name;
            try {
                FileOutputStream fos = new FileOutputStream(path);
                fos.write(file.getBytes()); // 写入文件
                lists.add(path);
                fos.close();
            } catch (Exception e) {
                System.out.println("Exception:" + e);
            }
        }


        return lists;
    }

    /**
     * @param path     excel路径
     * @param aimClass 实体类字节码对象
     * @param <T>      实体类
     * @return 封装excel的内容集合
     */
    public static <T> List<T> parseFromExcel(String path, Class<T> aimClass) {
        List<T> result = new ArrayList<T>();
        try {
            FileInputStream fis = new FileInputStream(path);
            Workbook workbook = WorkbookFactory.create(fis);
            //对excel文档的第一页,即sheet1进行操作
            Sheet sheet = workbook.getSheetAt(0);
            int lastRaw = sheet.getLastRowNum();
            for (int i = 2; i <= lastRaw; i++) {
                //第i行
                Row row = sheet.getRow(i);
                T object = aimClass.newInstance();
                Field[] fields = aimClass.getDeclaredFields();
                for (int j = 0; j < fields.length; j++) {
                    Field field = fields[j];
                    field.setAccessible(true);
                    Class<?> type = field.getType();
                    //第j列
                    Cell cell = row.getCell(j);
                    if (cell == null) {
                        continue;
                    }
                    //很重要的一行代码,如果不加,像12345这样的数字是不会给你转成String的,只会给你转成double,而且会导致cell.getStringCellValue()报错
                    cell.setCellType(STRING);
                    String cellContent = cell.getStringCellValue();
                    String empty = "";
                    if (empty.equals(cellContent.replace(" ", ""))) continue;
//                    cellContent = "".equals(cellContent) ? null : cellContent;
                    if (type.equals(String.class)) {
                        field.set(object, cellContent);
                    } else if (type.equals(char.class) || type.equals(Character.class)) {
                        field.set(object, cellContent.charAt(0));
                    } else if (type.equals(int.class) || type.equals(Integer.class)) {
                        field.set(object, Integer.parseInt(cellContent));
                    } else if (type.equals(long.class) || type.equals(Long.class)) {
                        field.set(object, Long.parseLong(cellContent));
                    } else if (type.equals(float.class) || type.equals(Float.class)) {
                        field.set(object, Float.parseFloat(cellContent));
                    } else if (type.equals(double.class) || type.equals(Double.class)) {
                        field.set(object, Double.parseDouble(cellContent));
                    } else if (type.equals(short.class) || type.equals(Short.class)) {
                        field.set(object, Short.parseShort(cellContent));
                    } else if (type.equals(byte.class) || type.equals(Byte.class)) {
                        field.set(object, Byte.parseByte(cellContent));
                    } else if (type.equals(boolean.class) || type.equals(Boolean.class)) {
                        field.set(object, Boolean.parseBoolean(cellContent));
                    } else if (type.equals(LocalDateTime.class)) {
                        GregorianCalendar calendar = new GregorianCalendar(1900, Calendar.JANUARY, -1);
                        try {
                            Date date = DateUtils.addDays(calendar.getTime(), Integer.parseInt(cellContent));
                            LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.ofOffset("GMT", ZoneOffset.ofHours(8)));
                            field.set(object, localDateTime);
                        } catch (Exception e) {
                        }
                    }
                }
                result.add(object);
            }
            fis.close();
            return result;
        } catch (
                Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
