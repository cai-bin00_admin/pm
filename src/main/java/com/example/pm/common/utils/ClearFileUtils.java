package com.example.pm.common.utils;

import com.example.pm.common.constant.SystemConstant;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class ClearFileUtils {

    //清除文件
    public static void clearFile() {


        //需清除文件地址
        String upload = SystemConstant.Upload;
        Path path = Paths.get(upload);

        //清除文件
        try {
            Files.walkFileTree(path,
                    new SimpleFileVisitor<Path>() {
                        // 先去遍历删除文件
                        @Override
                        public FileVisitResult visitFile(Path file,
                                                         BasicFileAttributes attrs) throws IOException {
                            Files.delete(file);
                            System.out.printf("文件被删除 : %s%n", file);
                            return FileVisitResult.CONTINUE;
                        }

                        // 再去遍历删除目录
                        @Override
                        public FileVisitResult postVisitDirectory(Path dir,
                                                                  IOException exc) throws IOException {
                            Files.delete(dir);
                            System.out.printf("文件夹被删除: %s%n", dir);
                            return FileVisitResult.CONTINUE;
                        }

                    }
            );
        } catch (IOException e) {
            System.out.println("文件已被删除或不存在");
        }
    }


}
