package com.example.pm.common.exception;

import com.example.pm.common.result.R;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * SaToken全局异常拦截
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 全局异常拦截，鉴权失败不会报错，会返回给前端报错原因
     */
    @ExceptionHandler
    public R<String> handlerException(Exception e) {
        String error = e.toString();
        if (error.contains("Token无效")) {
            //token无效
            return R.token("登录信息已过期，请重新登陆！");
        }
        if (error.contains("未能读取到有效Token")) {
            //未登录账号
            return R.token("请登录账号！");
        }
        if (error.contains("UndeclaredThrowableException")) {
            //无权限
            return R.fail(e.getCause().getMessage());
        }
        return R.warn(error);
    }


}

