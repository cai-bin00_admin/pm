package com.example.pm.common.config;


import cn.dev33.satoken.interceptor.SaInterceptor;
import com.example.pm.common.basic.perm.PermInterceptor;
import com.example.pm.common.constant.SystemConstant;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@Api(value = "拦截器", tags = {"拦截器"})
public class Configure implements WebMvcConfigurer {
    @Autowired
    private PermInterceptor permInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册路由拦截器，自定义认证规则
        registry.addInterceptor(new SaInterceptor(
//                handle -> {
////                SaRouter.match("/**").notMatch(SystemConstant.Interceptors).check(r -> StpUtil.checkLogin());        // 要执行的校验动作，可以写完整的 lambda 表达式
//                    SaRouter.match("/employee/**", r -> StpUtil.checkLogin());
//                }
                ).isAnnotation(true))
                //拦截所有接口
                .addPathPatterns("/**")
                //不拦截的接口
                .excludePathPatterns(SystemConstant.Interceptors);

//        registry.addInterceptor(permInterceptor).addPathPatterns("/**");

    }

    public void addCorsMappings(CorsRegistry registry) {
        // 设置允许跨域的路径
        registry.addMapping("/**")
                // 设置允许跨域请求的域名
                .allowedOriginPatterns("*")
                // 是否允许cookie
                .allowCredentials(true)
                // 设置允许的请求方式
                .allowedMethods("GET", "POST", "DELETE", "PUT")
                // 设置允许的header属性
                .allowedHeaders("*")
                // 跨域允许时间
                .maxAge(3600);
    }
}
