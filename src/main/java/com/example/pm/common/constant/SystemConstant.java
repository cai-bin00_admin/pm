package com.example.pm.common.constant;

import io.swagger.annotations.Api;

@Api(value = "系统常量", tags = {"系统常量"})
public class SystemConstant {
    /**
     * 最小account 十亿
     */
    public static final Integer billion = 1000000000;
    /**
     * 拦截器白名单
     */
    public static final String[] Interceptors = {"/code/", "/login/", "/login/register"};
    /**
     * log日志跳过的方法
     */
    public static final String[] passMethod = {"getAsideMenu", "getLogList"};
    /**
     * 不输出参数以及不记录参数
     */
    public static final String[] NotOutputAndRecord = {"/login/"};

    /**
     * 账号状态-正常激活
     */
    public static final String USER_STATUS_NORMAL = "激活";
    /**
     * 账号状态-禁止使用
     */
    public static final String USER_STATUS_DISABLE = "禁用";
    /**
     * 账号状态-暂时禁止
     */
    public static final String USER_STATUS_TEMPORARILY_DISABLE = "TEMPORARILY_DISABLE";
    /**
     * 账号密码最大错误次数
     */
    public static final Integer passwordErrorNum = 5;
    /**
     * 账号密码最大错误次数记录时间
     * 10分钟
     */
    public static final long passwordErrorNum_Time = 60 * 10;
    /**
     * 本地保存文件基地址
     */
    public static final String Upload = "C:\\upload\\";
    /**
     * 本地图片保存地址
     */
    public static final String IMAGE_Upload = Upload + "image\\";
    /**
     * 附件下载地址
     */
    public static final String FILE_Upload = Upload + "file\\";
    /**
     * 员工信息导出地址
     */
    public static final String exportEmployees = Upload + "exportEmployees\\";
    /**
     * 员工信息导出文件名称
     */
    public static final String exportEmployees_FileName = exportEmployees + "EmployeeInformationForm.xls";

    /**
     * 接口所在位置
     */
    public static final String ControllerPath = "com.example.pm.controller";
}
