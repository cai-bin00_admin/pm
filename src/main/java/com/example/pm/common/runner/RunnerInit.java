package com.example.pm.common.runner;


import com.example.pm.common.redis.RedisUtils;
import com.example.pm.common.utils.ClearFileUtils;
import com.example.pm.service.CarService;
import com.example.pm.service.PermService;
import com.example.pm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class RunnerInit {
    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private CarService carService;
    @Autowired
    private UserService userService;
    @Autowired
    private PermService permService;

    @Async
    @PostConstruct
    public void init() {
        redisUtils.delAll();
        System.out.println("===============检查数据库表===============");
        System.out.println("===============初始化权限===============");
        permService.createPermRedis();
        System.out.println("===============初始化车辆===============");
        carService.createCarRedis();
        System.out.println("===============初始化账号===============");
        userService.createUserRedis();
        System.out.println("===============初始化完成=================");
        ClearFileUtils.clearFile();
        System.out.println("===============文件清除完成=================");
    }

}
