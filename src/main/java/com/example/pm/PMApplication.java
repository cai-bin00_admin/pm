package com.example.pm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAsync
@EnableScheduling
@SpringBootApplication
public class PMApplication {

    public static void main(String[] args) {
        SpringApplication.run(PMApplication.class, args);
        System.out.println(
                "              (♥◠‿◠)ﾉﾞ         启动成功         ლ(´ڡ`ლ)ﾞ              \n" +
                        " ____  __._____.___.____  ______________      ___________________ \n" +
                        "|    |/ _|\\__  |   |\\   \\/  /\\__    ___/      \\_   ___ \\______   \\\n" +
                        "|      <   /   |   | \\     /   |    |  ______ /    \\  \\/|    |  _/\n" +
                        "|    |  \\  \\____   | /     \\   |    | /_____/ \\     \\___|    |   \\\n" +
                        "|____|__ \\ / ______|/___/\\  \\  |____|          \\______  /______  /\n" +
                        "        \\/ \\/             \\_/                         \\/       \\/ ");
    }

}
