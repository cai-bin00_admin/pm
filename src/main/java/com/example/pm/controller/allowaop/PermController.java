package com.example.pm.controller.allowaop;


import com.example.pm.common.basic.MultiRequestBody.MultiRequestBody;
import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.domain.Perm;
import com.example.pm.service.PermService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "权限管理", tags = {"权限管理"})
@RequestMapping(value = "/perm", method = {RequestMethod.POST, RequestMethod.GET})
public class PermController {
    @Autowired
    private PermService permService;

    @ResponseBody
    @ApiOperation("获取权限列表")
    @RequestMapping("/getList")
    @PermInter(perm = ":getList:perm", name = "获取权限列表", jsjb = "1")
    public R<List<Perm>> getList() {
        return R.ok(permService.getList());
    }

    @ResponseBody
    @ApiOperation("搜索权限")
    @RequestMapping("/getListByZh/{zh}")
//    @PermInter(perm = ":searchList:perm", name = "搜索权限列表", jsjb = "2")
    public R<List<String>> getCarBySearch(@PathVariable("zh") String zh) {
        return R.ok(permService.getListByZh(zh));
    }

    @ResponseBody
    @ApiOperation("更改某个账号的权限")
    @RequestMapping("/changePermForEmployee")
    @PermInter(perm = ":changePerm:forEmployee", name = "更改某个账号的权限", jsjb = "1")
    public R<String> changePermForEmployee(@MultiRequestBody String zh,
                                           @MultiRequestBody List<String> perm) {
        return permService.changePermForEmployee(zh, perm);
    }
}
