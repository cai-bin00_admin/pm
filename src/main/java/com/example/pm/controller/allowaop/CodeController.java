package com.example.pm.controller.allowaop;


import com.example.pm.common.result.R;
import com.example.pm.service.CodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@Api(value = "验证码管理", tags = {"验证码管理"})
@RequestMapping(value = "/code", method = {RequestMethod.POST, RequestMethod.GET})
public class CodeController {
    @Autowired
    private CodeService codeService;

    @ResponseBody
    @RequestMapping("/")
    @ApiOperation("获取验证码")
    public R<Map<String, Object>> getCode() {
        return codeService.getCode();
    }
}
