package com.example.pm.controller.allowaop;


import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Log;
import com.example.pm.domain.Vo.PageVo;
import com.example.pm.service.LogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value = "日志管理", tags = {"日志管理"})
@RequestMapping(value = "/log", method = {RequestMethod.POST, RequestMethod.GET})
public class LogController {
    @Autowired
    private LogService logService;

    @ResponseBody
    @ApiOperation("获取操作记录")
    @RequestMapping("/getList")
    @PermInter(perm = ":getList:log", name = "获取操作记录", jsjb = "1")
    public R<PageVo<Log>> getLogList(@RequestBody PageBo pageBo) {
        return R.ok(logService.getList(pageBo));
    }
}
