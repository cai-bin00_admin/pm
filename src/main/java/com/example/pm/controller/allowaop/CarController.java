package com.example.pm.controller.allowaop;


import com.example.pm.common.basic.MultiRequestBody.MultiRequestBody;
import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Car;
import com.example.pm.domain.Vo.PageVo;
import com.example.pm.service.CarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value = "车辆管理", tags = {"车辆管理"})
@RequestMapping(value = "/car", method = {RequestMethod.POST, RequestMethod.GET})
public class CarController {
    @Autowired
    private CarService carService;

    @ResponseBody
    @ApiOperation("获取车辆列表")
    @RequestMapping("/getCars")
    @PermInter(perm = ":getList:car", name = "获取车辆列表", jsjb = "2")
    public R<PageVo<Car>> getCars(@RequestBody PageBo pageBo) {
        return R.ok(carService.getCars(pageBo));
    }

    @ResponseBody
    @ApiOperation("搜索车辆")
    @RequestMapping("/getCarBySearch")
    @PermInter(perm = ":searchList:car", name = "搜索车辆列表", jsjb = "2")
    public R<PageVo<Car>> getCarBySearch(@MultiRequestBody Car car, @MultiRequestBody PageBo pageBo) {
        return R.ok(carService.getCarBySearch(car, pageBo));
    }
}
