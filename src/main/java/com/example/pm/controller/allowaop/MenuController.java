package com.example.pm.controller.allowaop;


import com.example.pm.common.basic.MultiRequestBody.MultiRequestBody;
import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Menu;
import com.example.pm.domain.Vo.PageVo;
import com.example.pm.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "菜单管理", tags = {"菜单管理"})
@RequestMapping(value = "/menu", method = {RequestMethod.POST, RequestMethod.GET})
public class MenuController {
    @Autowired
    private MenuService menuService;

    @ResponseBody
    @ApiOperation("左侧菜单")
    @RequestMapping("/aside")
    public R<List<Menu>> getAsideMenu() {
        return R.ok(menuService.getAsideMenu());
    }

    @ResponseBody
    @ApiOperation("菜单列表-分页")
    @RequestMapping("/getPage")
    @PermInter(perm = ":getPage:menu", name = "获取菜单列表并进行分页", jsjb = "1")
    public R<PageVo<Menu>> getPage(@RequestBody PageBo pageBo) {
        return R.ok(menuService.getPage(pageBo));
    }

    @ResponseBody
    @ApiOperation("父菜单列表")
    @RequestMapping("/listParent")
    @PermInter(perm = ":getList:menu:parent", name = "获取父菜单列表")
    public R<List<Menu>> listParent() {
        return R.ok(menuService.listParent());
    }

    @ResponseBody
    @ApiOperation("添加菜单")
    @RequestMapping(path = "/insert")
    @PermInter(perm = ":insert:menu", name = "添加菜单", jsjb = "1")
    public R<PageVo<Menu>> insert(@MultiRequestBody Menu menu, @MultiRequestBody PageBo pageBo) {
        return menuService.insert(menu, pageBo);
    }

    @ResponseBody
    @ApiOperation("更新菜单")
    @RequestMapping("/update")
    @PermInter(perm = ":update:menu", name = "更新菜单", jsjb = "1")
    public R<PageVo<Menu>> update(@MultiRequestBody Menu menu, @MultiRequestBody PageBo pageBo) {
        return menuService.update(menu, pageBo);
    }

    @ResponseBody
    @ApiOperation("删除菜单")
    @RequestMapping("/delete")
    @PermInter(perm = ":delete:menu", name = "删除菜单", jsjb = "1")
    public R<PageVo<Menu>> delete(@MultiRequestBody Menu menu, @MultiRequestBody PageBo pageBo) {
        return menuService.delete(menu, pageBo);
    }

    @ResponseBody
    @ApiOperation("菜单管理的模糊查询")
    @RequestMapping("/getPage/search")
    @PermInter(perm = ":searchPage:menu", name = "模糊查询菜单", jsjb = "1")
    public R<PageVo<Menu>> getPageBySearch(@MultiRequestBody Menu menu, @MultiRequestBody PageBo pageBo) {
        PageVo<Menu> menus = menuService.getPageBySearch(menu, pageBo);
        return R.ok(menus);
    }

    @ResponseBody
    @ApiOperation("根据账号获取菜单列表的ID")
    @RequestMapping("/getListByZh/{zh}")
    @PermInter(perm = ":searchList:menuId:ByZh", name = "根据账号获取菜单列表的ID")
    public R<List<Integer>> getListByZh(@PathVariable("zh") String zh) {
        return menuService.getListByZh(zh);
    }

    @ResponseBody
    @ApiOperation("菜单列表")
    @RequestMapping("/getList")
    @PermInter(perm = ":getList:menu", name = "获取菜单列表")
    public R<List<Menu>> getList() {
        return menuService.getList();
    }

    @ResponseBody
    @ApiOperation("修改某一账号的菜单列表")
    @RequestMapping("/changeMenuForEmployee")
    @PermInter(perm = ":changeMenu:forEmployee", name = "修改某一账号的菜单列表", jsjb = "1")
    public R<String> changeMenuForEmployee(@MultiRequestBody String zh,
                                           @MultiRequestBody List<Integer> newMenu,
                                           @MultiRequestBody List<Integer> oldMenu) {
        return menuService.changeMenuForEmployee(zh, newMenu, oldMenu);
    }

}
