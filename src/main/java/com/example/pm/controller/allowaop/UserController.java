package com.example.pm.controller.allowaop;


import com.example.pm.common.basic.MultiRequestBody.MultiRequestBody;
import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.User;
import com.example.pm.domain.Vo.PageVo;
import com.example.pm.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@Api(value = "账号管理", tags = {"账号管理"})
@RequestMapping(value = "/user", method = {RequestMethod.POST, RequestMethod.GET})
public class UserController {
    @Autowired
    private UserService userService;

    @ResponseBody
    @ApiOperation("获取账号列表")
    @RequestMapping("/getUserPage")
    @PermInter(perm = ":getPage:user", name = "获取账号列表并分页", jsjb = "1")
    public R<PageVo<User>> getUserPage(@RequestBody PageBo pageBo) {
        return R.ok(userService.getUserPage(pageBo));
    }

    @ResponseBody
    @ApiOperation("搜索账号")
    @RequestMapping("/getUserBySearch")
    @PermInter(perm = ":search:user", name = "搜索账号", jsjb = "2")
    public R<Object> getUserBySearch(@MultiRequestBody User user, @MultiRequestBody PageBo pageBo) {
        return userService.getUserBySearch(user, pageBo);
    }

    @ResponseBody
    @ApiOperation("搜索账号头像")
    @RequestMapping("/userImage")
    @PermInter(perm = ":image:user", name = "搜索账号头像")
    public R<byte[]> getUserImage() {
        return R.ok(userService.getUserImage());
    }

    @ResponseBody
    @RequestMapping("/getInfo")
    @ApiOperation("主页回显账号信息")
    @PermInter(perm = ":getInfo:Index", name = "主页回显账号信息")
    public R<Map<String, Object>> getInfo() {
        return R.ok(userService.getInfo());
    }

    @ResponseBody
    @ApiOperation("更改账号状态")
    @RequestMapping("/changeZt")
    @PermInter(perm = ":change:User:ZT", name = "更改账号状态", jsjb = "1")
    public R<Object> changeZt(@MultiRequestBody String zh, @MultiRequestBody PageBo pageBo) {
        return userService.changeZt(zh, pageBo);
    }

    @ResponseBody
    @ApiOperation("更新员工账号的角色")
    @RequestMapping("/changeRole")
    public R<String> changeRole(@RequestBody User user) {
        return userService.changeRole(user);
    }

}
