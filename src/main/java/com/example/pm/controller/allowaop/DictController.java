package com.example.pm.controller.allowaop;


import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.domain.Dict;
import com.example.pm.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "字典管理", tags = {"字典管理"})
@RequestMapping(value = "/dict", method = {RequestMethod.POST, RequestMethod.GET})
public class DictController {
    @Autowired
    private DictService dictService;

    @ResponseBody
    @ApiOperation("获取字典")
    @RequestMapping("/getDict/{dict}")
    @PermInter(perm = ":getList:dict", name = "获取字典")
    public R<List<Dict>> getDict(@PathVariable("dict") String dict) {
        return R.ok(dictService.getDict(dict));
    }

}
