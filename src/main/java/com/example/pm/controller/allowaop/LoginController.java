package com.example.pm.controller.allowaop;


import cn.dev33.satoken.annotation.SaIgnore;
import cn.dev33.satoken.stp.StpUtil;
import com.example.pm.common.basic.MultiRequestBody.MultiRequestBody;
import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.domain.Bo.LoginBo;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Login;
import com.example.pm.domain.Vo.PageVo;
import com.example.pm.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value = "登录管理", tags = {"登录管理"})
@RequestMapping(value = "/login", method = {RequestMethod.POST, RequestMethod.GET})
public class LoginController {
    @Autowired
    private LoginService loginService;

    @ResponseBody
    @ApiOperation("登录")
    @RequestMapping("/")
    public R<Object> doLogin(@RequestBody LoginBo loginBo) {
        return loginService.doLogin(loginBo);
    }

    @SaIgnore
    @ResponseBody
    @ApiOperation("退出")
    @RequestMapping("/out")
    public R<String> loginOut() {
        StpUtil.logout();
        return R.ok("退出成功");
    }

    @ResponseBody
    @ApiOperation("获取登录记录")
    @RequestMapping("/getLoginList")
    @PermInter(perm = ":getList:login", name = "获取登录记录", jsjb = "1")
    public R<PageVo<Login>> getLoginList(@RequestBody PageBo pageBo) {
        return R.ok(loginService.getLoginList(pageBo));
    }

    @ResponseBody
    @ApiOperation("主页回显登录记录")
    @RequestMapping("/getList/Index")
    @PermInter(perm = ":getList:index", name = "获取登录记录")
    public R<List<Login>> getListIndex() {
        return R.ok(loginService.getListIndex());
    }

    @ResponseBody
    @ApiOperation("搜索登录记录")
    @RequestMapping("/getLoginList/search")
    @PermInter(perm = ":searchList:login", name = "搜索登录记录", jsjb = "1")
    public R<PageVo<Login>> getLoginListBySearch(@MultiRequestBody Login login, @MultiRequestBody PageBo pageBo) {
        return loginService.getLoginListBySearch(login, pageBo);
    }

}
