package com.example.pm.controller.allowaop;


import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.domain.Notice;
import com.example.pm.service.NoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(value = "系统通知管理", tags = {"系统通知管理"})
@RequestMapping(value = "/notice", method = {RequestMethod.POST, RequestMethod.GET})
public class NoticeController {
    @Autowired
    private NoticeService noticeService;

    @ResponseBody
    @RequestMapping("/")
    @ApiOperation("获取系统通知")
    @PermInter(perm = ":getList:notice", name = "获取系统通知列表")
    public R<List<Notice>> getNotice() {
        return noticeService.getNotice();
    }
}
