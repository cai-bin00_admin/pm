package com.example.pm.controller.allowaop;


import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.service.CountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@Api(value = "数据统计管理", tags = {"数据统计管理"})
@RequestMapping(value = "/count", method = {RequestMethod.POST, RequestMethod.GET})
public class CountController {

    @Autowired
    private CountService countService;

    @ResponseBody
    @ApiOperation("各项数据-图表")
    @RequestMapping("/TextTable")
    @PermInter(perm = ":table:text", name = "统计图表:数字图表")
    public R<Map<String, Object>> TextTable() {
        return R.ok(countService.TextTable());
    }

    @ResponseBody
    @ApiOperation("今年每月份新来员工-折线图")
    @RequestMapping("/NewEmployeesEveryMonthThisYear")
    @PermInter(perm = ":table:newEmployee:month", name = "统计图表:今年每月份新来员工")
    public R<Map<String, Object>> NewEmployeesEveryMonthThisYear() {
        return R.ok(countService.NewEmployeesEveryMonthThisYear());
    }

    @ResponseBody
    @ApiOperation("每年新来员工-折线图")
    @RequestMapping("/NewEmployeesEveryYear")
    @PermInter(perm = ":table:newEmployee:year", name = "统计图表:每年新来员工")
    public R<Map<String, Object>> NewEmployeesEveryYear() {
        return R.ok(countService.NewEmployeesEveryYear());
    }


}

