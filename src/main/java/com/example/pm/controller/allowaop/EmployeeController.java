package com.example.pm.controller.allowaop;


import com.example.pm.common.basic.MultiRequestBody.MultiRequestBody;
import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.domain.Bo.PageBo;
import com.example.pm.domain.Employee;
import com.example.pm.domain.Image;
import com.example.pm.domain.Vo.PageVo;
import com.example.pm.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value = "员工管理", tags = {"员工管理"})
@RequestMapping(value = "/employee", method = {RequestMethod.POST, RequestMethod.GET})
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @ResponseBody
    @ApiOperation("获取在职员工列表")
    @RequestMapping("/getEmployees")
    @PermInter(perm = ":getList:employee", name = "获取在职员工列表", jsjb = "2")
    public R<PageVo<Object>> getEmployees(@RequestBody PageBo pageBo) {
        return employeeService.getEmployees(pageBo, "在职");
    }

    @ResponseBody
    @ApiOperation("搜索在职员工列表")
    @RequestMapping("/getEmployeeBySearch")
    @PermInter(perm = ":searchList:employee", name = "搜索在职员工列表", jsjb = "2")
    public R<PageVo<Object>> getEmployeeBySearch(@MultiRequestBody Employee employee, @MultiRequestBody PageBo pageBo) {
        return employeeService.getEmployeeBySearch(employee, pageBo, "在职");
    }

    @ResponseBody
    @ApiOperation("获取离职员工列表")
    @RequestMapping("/getDepartEmployees")
    @PermInter(perm = ":getList:employee:depart", name = "获取离职员工列表", jsjb = "2")
    public R<PageVo<Object>> getDepartEmployees(@RequestBody PageBo pageBo) {
        return employeeService.getEmployees(pageBo, "离职");
    }

    @ResponseBody
    @ApiOperation("搜索离职员工列表")
    @RequestMapping("/getDepartEmployeeBySearch")
    @PermInter(perm = ":searchList:employee:depart", name = "搜索离职员工列表", jsjb = "2")
    public R<PageVo<Object>> getDepartEmployeeBySearch(@MultiRequestBody Employee employee, @MultiRequestBody PageBo pageBo) {
        return employeeService.getEmployeeBySearch(employee, pageBo, "离职");
    }

    @ResponseBody
    @ApiOperation("获取临时员工列表")
    @RequestMapping("/getTemporaryEmployees")
    @PermInter(perm = ":getList:employee:temporary", name = "获取临时员工列表", jsjb = "2")
    public R<PageVo<Object>> getTemporaryEmployees(@RequestBody PageBo pageBo) {
        return employeeService.getEmployees(pageBo, "临时");
    }

    @ResponseBody
    @ApiOperation("搜索临时员工列表")
    @RequestMapping("/getTemporaryEmployeeBySearch")
    @PermInter(perm = ":searchList:employee:temporary", name = "搜索临时员工列表", jsjb = "2")
    public R<PageVo<Object>> getTemporaryEmployeeBySearch(@MultiRequestBody Employee employee, @MultiRequestBody PageBo pageBo) {
        return employeeService.getEmployeeBySearch(employee, pageBo, "临时");
    }

    @ResponseBody
    @ApiOperation("员工搜索选项")
    @RequestMapping("/EmployeeSearchOption")
    @PermInter(perm = ":searchOption:employee", name = "员工搜索选项", jsjb = "2")
    public R<Object> EmployeeSearchOption() {
        return employeeService.EmployeeSearchOption();
    }

    @ResponseBody
    @ApiOperation("新增员工")
    @RequestMapping("/insertEmployee")
    @PermInter(perm = ":insert:employee", name = "新增员工", jsjb = "2")
    public R<Object> insertEmployee(@MultiRequestBody Employee employee, @MultiRequestBody PageBo pageBo) {
        return employeeService.insertEmployee(employee, pageBo);
    }

    @ResponseBody
    @ApiOperation("删除员工")
    @RequestMapping("/deleteEmployee/{id}")
    @PermInter(perm = ":delete:employee", name = "删除员工", jsjb = "2")
    public R<Object> delete(@PathVariable("id") String id, @RequestBody PageBo pageBo) {
        return employeeService.deleteEmployee(id, pageBo);
    }

    @ResponseBody
    @ApiOperation("修改员工信息")
    @RequestMapping("/updateEmployee")
    @PermInter(perm = ":update:employee", name = "修改员工信息", jsjb = "2")
    public R<PageVo<Object>> updateEmployee(@MultiRequestBody Employee employee, @MultiRequestBody PageBo pageBo) {
        return employeeService.updateEmployee(employee, pageBo);
    }

    @ResponseBody
    @RequestMapping("/getXxdjb/{id}")
    @ApiOperation("获取员工的信息登记表图片")
    @PermInter(perm = ":getImage:xxdjb", name = "获取员工的信息登记表图片", jsjb = "2")
    public R<Object> getXxdjb(@PathVariable("id") Integer id) {
        return employeeService.getXxdjb(id);
    }


    @ResponseBody
    @RequestMapping("/insertXxdjb")
    @ApiOperation("添加员工的信息登记表图片")
    @PermInter(perm = ":insertImage:xxdjb", name = "添加员工的信息登记表图片", jsjb = "2")
    public R<Object> insertXxdjb(@RequestBody Image image) {
        return employeeService.insertXxdjb(image);
    }

    @ResponseBody
    @RequestMapping("/getXsxwzs/{id}")
    @ApiOperation("获取员工的学士学位证书图片")
    @PermInter(perm = ":getImage:xsxwzs", name = "获取员工的学士学位证书图片", jsjb = "2")
    public R<Object> getXsxwzs(@PathVariable("id") Integer id) {
        return employeeService.getXsxwzs(id);
    }

    @ResponseBody
    @RequestMapping("/insertXsxwzs")
    @ApiOperation("添加员工的学士学位证书图片")
    @PermInter(perm = ":insertImage:xsxwzs", name = "添加员工的学士学位证书图片", jsjb = "2")
    public R<Object> insertXsxwzs(@RequestBody Image image) {
        return employeeService.insertXsxwzs(image);
    }

    @ResponseBody
    @RequestMapping("/getByzs/{id}")
    @ApiOperation("获取员工的毕业证书图片")
    @PermInter(perm = ":getImage:byzs", name = "获取员工的毕业证书图片", jsjb = "2")
    public R<Object> getByzs(@PathVariable("id") Integer id) {
        return employeeService.getByzs(id);
    }

    @ResponseBody
    @RequestMapping("/insertByzs")
    @ApiOperation("添加员工的毕业证书图片")
    @PermInter(perm = ":insertImage:byzs", name = "添加员工的毕业证书图片", jsjb = "2")
    public R<Object> insertByzs(@RequestBody Image image) {
        return employeeService.insertByzs(image);
    }

    @ResponseBody
    @RequestMapping("/getZczs/{id}")
    @ApiOperation("获取员工的职称图片")
    @PermInter(perm = ":getImage:zczs", name = "获取员工的职称图片", jsjb = "2")
    public R<Object> getZczs(@PathVariable("id") Integer id) {
        return employeeService.getZczs(id);
    }

    @ResponseBody
    @RequestMapping("/insertZczs")
    @ApiOperation("添加员工的职称证书图片")
    @PermInter(perm = ":insertImage:zczs", name = "添加员工的职称证书图片", jsjb = "2")
    public R<Object> insertZczs(@RequestBody Image image) {
        return employeeService.insertZczs(image);
    }

    @ResponseBody
    @RequestMapping("/getSfz/{id}")
    @ApiOperation("获取员工的身份证图片")
    @PermInter(perm = ":getImage:sfz", name = "获取员工的身份证图片", jsjb = "2")
    public R<Object> getSfz(@PathVariable("id") Integer id) {
        return employeeService.getSfz(id);
    }

    @ResponseBody
    @RequestMapping("/insertSfz")
    @ApiOperation("添加员工的身份证图片")
    @PermInter(perm = ":insertImage:sfz", name = "添加员工的身份证图片", jsjb = "2")
    public R<Object> insertSfz(@RequestBody Image image) {
        return employeeService.insertSfz(image);
    }

    @ResponseBody
    @RequestMapping("/getJsb/{id}")
    @ApiOperation("获取员工的驾驶本图片")
    @PermInter(perm = ":getImage:jsb", name = "获取员工的驾驶本图片", jsjb = "2")
    public R<Object> getJsb(@PathVariable("id") Integer id) {
        return employeeService.getJsb(id);
    }

    @ResponseBody
    @RequestMapping("/insertJsb")
    @ApiOperation("添加员工的驾驶本图片")
    @PermInter(perm = ":insertImage:jsb", name = "添加员工的驾驶本图片", jsjb = "2")
    public R<Object> insertJsb(@RequestBody Image image) {
        return employeeService.insertJsb(image);
    }

}
