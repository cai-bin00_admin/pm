package com.example.pm.controller.allowaop;


import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.domain.Role;
import com.example.pm.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@Api(value = "角色管理", tags = {"角色管理"})
@RequestMapping(value = "/role", method = {RequestMethod.POST, RequestMethod.GET})
public class RoleController {
    @Autowired
    private RoleService roleService;

    @ResponseBody
    @ApiOperation("获取角色种类")
    @RequestMapping("/getRoleMap")
    @PermInter(perm = ":getMap:Role", name = "获取角色种类", jsjb = "2")
    public R<List<Map<String, Object>>> getRoleMap() {
        return R.ok(roleService.getRoleMap());
    }

    @ResponseBody
    @ApiOperation("获取角色列表")
    @RequestMapping("/getRoleList")
    @PermInter(perm = ":getList:Role", name = "获取角色列表", jsjb = "2")
    public R<List<Role>> getRoleList() {
        return roleService.getRoleList();
    }

    @ResponseBody
    @ApiOperation("获取账号的角色信息")
    @RequestMapping("/getRoleIdByZh/{zh}")
    @PermInter(perm = ":get:RoleId:byZh", name = "获取账号的角色信息", jsjb = "1")
    public R<Integer> getRoleIdByZh(@PathVariable("zh") String zh) {
        return roleService.getRoleIdByZh(zh);
    }

}
