package com.example.pm.controller.noallowaop;


import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.service.ImportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Api(value = "导入管理", tags = {"导入管理"})
@RequestMapping(value = "/import", method = {RequestMethod.POST, RequestMethod.GET})
public class ImportController {
    @Autowired
    private ImportService importEmployees;

    @ResponseBody
    @ApiOperation("导入用户xls")
    @RequestMapping(value = "/employee", method = RequestMethod.POST)
    @PermInter(perm = ":import:employee", name = "导入xls表格,生成用户信息", jsjb = "1")
    public R<String> importEmployees(MultipartFile[] files) {
        return importEmployees.importEmployees(files);
    }

    @ResponseBody
    @ApiOperation("导入车辆xls")
    @RequestMapping(value = "/car", method = RequestMethod.POST)
    @PermInter(perm = ":import:car", name = "导入xls表格,生成车辆信息", jsjb = "1")
    public R<String> importCars(MultipartFile[] files) {
        return importEmployees.importCars(files);
    }

}
