package com.example.pm.controller.noallowaop;


import com.example.pm.common.basic.perm.PermInter;
import com.example.pm.common.result.R;
import com.example.pm.service.ExportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@Api(value = "导出管理", tags = {"导出管理"})
@RequestMapping(value = "/export", method = {RequestMethod.POST, RequestMethod.GET})
public class exportController {
    @Autowired
    private ExportService exportEmployees;

    @ResponseBody
    @ApiOperation("导出用户xls")
    @RequestMapping(value = "/employee", method = RequestMethod.POST)
    @PermInter(perm = ":export:employee", name = "导出用户信息,并生成xls表格", jsjb = "1")
    public R<Map<String, Object>> exportEmployees(@RequestBody List<Integer> ids) {
        return exportEmployees.exportEmployees(ids);
    }

    @ResponseBody
    @ApiOperation("导出车辆xls")
    @RequestMapping(value = "/car", method = RequestMethod.POST)
    @PermInter(perm = ":export:car", name = "导出车辆信息,并生成xls表格", jsjb = "1")
    public R<Map<String, Object>> exportCars() {
        return exportEmployees.exportCars();
    }

}
